run python script newcard.py    
            There are various CL options to change mass, ebeam energy, path directories to param, process, run cards and other options.
New folder "output" will be created 
run madgraph using ./bin/mg5_aMC PATH_TO_FILE/run.mg5 to run event with new cards



Instalation
---
Install madgraph and delphes following the instructions in:
https://twiki.cern.ch/twiki/bin/view/CMSPublic/MadgraphTutorial

You would also require pythia 8 which can be install by running the folling commands (Doesn't work in OSX)
```
bin/mg5_aMC
install pythia8
install hepmc
install mg5amc_py8_interface
```

Then checkout this packages preferably inside the madgraph directory


RunCommands
---
To run the commands you can follow the follwing example:
```bash
DIR=chargino-proj/cards/
RUNCRD=${DIR}run_card.dat
PROCCRD=${DIR}chargino.dat
PARCARD=${DIR}param_card.dat

echo python3 newcard.py -r ${RUNCRD} -P ${PROCCRD} -p ${PARCARD} 
python3 newcard.py -r ${RUNCRD} -P ${PROCCRD} -p ${PARCARD} -c 3
```
This will create the necessary runcards for madgraph in the output directory
to run madgraph you can call
```bash
bin/mg5_aMC output/run/run.mg5
```
 

Installion guide for Mac Users (Required for latest macosx, might not be necessary for old ones)
---
To install delphes and other packages locally in OSX you might need to add the following command to your bashrc or call this command before compiling if you are getting an error about your OSX version.
```bash
export MACOSX_DEPLOYMENT_TARGET=10.9
```

For Deplhes (Need export MACOSX_DEPLOYMENT_TARGET=10.7 or bigger)
```bash
<Downlaod latest delphes from https://cp3.irmp.ucl.ac.be/projects/delphes/>
tar -xzf Delphes-3.4.2.tar.gz
cd Delphes-3.4.2/
make
```

For hepMC (Need export MACOSX_DEPLOYMENT_TARGET=10.9)
```bash
git clone ssh://git@gitlab.cern.ch:7999/hepmc/HepMC.git
cd HepMC
mkdir build
cd build
cmake ../
make
make install
```

For pythia8
```bash
./configure --cxx-common="-stdlib=libc++ -O2 -pedantic -W -Wall -Wshadow -fPIC" --with-hepmc2-lib="<hepMCDirectory>/build/lib/"
make
make install
python compile.py <phytia8 directory> 
```

After installing all the listed programs don't forget to edit <madgraphDirectory>/nput/mg5_configuration.txt file to point to right locations. 
